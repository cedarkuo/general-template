# Changelogs

## v0.1.0

Features:

- 新增 [.editorconfig-lint-template](https://gitlab.kkinternal.com/gitlab-ci-template/general-template/blob/v0.1.0/editorconfig.yml)

## v0.2.0

Features:

- 新增 [.git-conflicts-detect](git-conflicts-detect.yml)
- 新增 [.shellcheck](shellcheck.yml)
- 新增 [.shfmt](shfmt.yml)
- 新增 [.yaml-lint](yaml-lint.yml)
- 新增 [.yamllint](yamllint.yml)

## v0.3.0

Breaking Changes:

- editorconfig.yml 更改檔案名稱，改成 [editorconfig-lint.yml](editorconfig-lint.yml)
- .editorconfig-lint-template 更改名稱，改成 [.editorconfig-lint](editorconfig-lint.yml)

## v0.4.0

Features:

- Add [.json-lint](json-lint.yml)

## v0.5.0

Breaking Changes:

- Update naming from json-lint to .json-lint [.json-lint](json-lint.yml)

## v0.6.0

Updates:

- Ignore .git folder in [.yamllint](test/yamllint.yml)

## v0.7.0

Fixes:

- Fix run command error when empty string sent to xargs

## v0.8.0

Fixes:

- Fix gitlab-ci template path
- Remove unused runner tags

## v0.9.0

Breaking Changes:

- Update template files docker image version
- Remove unused templates: `.yaml-lint` (duplicate of `.yamllint`)

## v0.10.0

Breaking Changes::

- Rename template json-lint to jsonlint

Updates:

- Update README.md

## v0.11.0

Features:
- Add [.markdownlint](markdownlint.yml)
- Add [.mardown-link-check](mardown-link-check.yml)
